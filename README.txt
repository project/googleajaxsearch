Description
-----------

The main googleajaxsearch module allows you to add inline searches over a number
of Google services (Web Search, Local Search, Video Search, Blog Search, News Search,
Book Search, Image Search and Patent Search).

Installation
------------

1) copy the googleajaxsearch directory into the modules directory

2) Get a Google Ajax Search API Key at http://code.google.com/apis/ajaxsearch/signup.html

3) enable the 'googleajaxsearch' module in drupal

4) edit admin/settings/googleajaxsearch to include the key you received from google and
   change any other default setting.

5) configure an 'input format' so that the gmap filter is activated.  If
   this will be on a 'html filtered' format, ensure that the weighting is
   such that the HTML filter comes before the gmap filter.


Instructions
------------

Macro
-----

A googleajaxsearch macro can be created on the googleajaxsearch/macro page and the text can then be
copied and pasted into any node where the gmap filter is enabled.

Default settings will be the initial settings and will be used for any
parameter not inserted into the macro.

Current macro flags
-------------------
target={self,blank,parent,top}
state={open,closed,partial}
mode={linear,tabbed}
searcher={web,video,news,blog,local}
initialsearch={some initial search string}
resultid={block element id name where the results should be placed}
place={initial local search place ex. "New York,NY"}

Macro Example
-------------
[googleajaxsearch|target=blank|state=closed|mode=linear|searcher=web|searcher=blog|searcher=video|searcher=local|initialsearch=NYC|resultid=somedivid|place=New York,NY]

Module functionality
--------------------

- googleajaxsearch block in the block section
- block title enable/disable
- custom block title
- available google inline searchers: 
	*	web search
	*	blog search
	*	news search
	* 	video search
	*	local search
- initial search phrase
- initial place for local search
- google control modes: 
	*	tabbed
	*	linear
- google searchers default state
	*	open
	*	closed
	*	partial
- link target:
	* 	blank
	*	self
	*	top
	*	parent
- enable search from root [search form in a different place than result]


User
----

Any user that has permission for "view googleajaxsearch" will then be able to see
a inline searcher and user google inline search.

Demo
----
For a few pages that show some of the items mentioned above see:
http://www.6na9.info/googleajaxsearch/macro
http://www.6na9.info/gasdemo

ugs & quirks
-------------

- when using googleajax search in sidebar video links and blogs links are too long which causes sidebar width change 

To do
-----

- create php functions to parse array and macro directly in theme file
- copy functionality has to be implemented so users can copy search results into the nodes directly
- search result nodes [different node type for storing users search results]
- macro creator improvments
- Create an API that will allow the use of the macro creation tool in any
  module.

Credit
------

Written by:
Meant4.pl development team:
Maciej Perlinski, Adam Ludwinski, Krzysztof Golinski
http://www.meant4.pl

Sugestions&Questions 
--------------------


History
-------

2006-10-26 - Initial release